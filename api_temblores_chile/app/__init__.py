from flask import Flask


app = Flask(__name__)

app.config.from_object('config.DevelopmentConfig')


from app.api.view import api


app.register_blueprint(api)