from flask import Blueprint, render_template
import requests

from app import app


api = Blueprint('api', __name__)


@api.route('/')
def index():

    datos_obtenidos = requests.get('https://api-sismologia-chile.herokuapp.com/')

    datos_formato_json = datos_obtenidos.json()

    #print(datos_formato_json)

    return render_template('index.html', datos=datos_formato_json)